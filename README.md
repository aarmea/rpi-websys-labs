Assignments for RPI CSCI-4962/ITWS-2110 Web Systems Development
===============================================================

This repository contains the assignments for Richard Plotka's course in Web
Systems Development and [Albert Armea][aarmea]'s attempts to complete them.

[aarmea]: http://www.albertarmea.com

Warning
-------

This repository contains (working or non-working) solutions to homework
assignments. Unless the assignments have been replaced, I strongly discourage
downloading and reading this code if you are currently taking or plan to take
this course.

Testing locally
---------------

The repository contains a Vagrant configuration for a server virtual machine
that can run locally within your computer running Windows, Mac OS X, or Linux.
To use it, download and install [VirtualBox][virtualbox] and
[Vagrant][vagrant], then in a terminal `cd` to this repository and run
`vagrant up`. Once Vagrant finishes starting the virtual machine, add

    127.0.0.1 www.dev-site.com dev-site.com dev.dev-site-static.com

to your `hosts` file, open a Web browser and navigate to
<http://www.dev-site.com:8080/>. When you're done, run
`vagrant suspend` to stop the virtual machine.

The Vagrant configuration is based on the LAMP stack at
<https://github.com/ymainier/vagrant-lamp> and includes
[PHP Composer][composer] using components from
[vagrant-chef-composer][chef-composer].

[virtualbox]: https://www.virtualbox.org/
[vagrant]: http://www.vagrantup.com/
[composer]: http://getcomposer.org/
[chef-composer]: https://github.com/Version2beta/vagrant-chef-composer
