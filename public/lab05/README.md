Lab 5: jQuery, JSON, and AJAX
=============================

Albert Armea <armeaa@rpi.edu>

Basic notes
-----------

This site can be used as an index of my labs. There is a header containg the
title and a footer containing some notes. On load, some JavaScript populates a
jQuery UI menu listing all the labs. Clicking one of these labs changes the
title and description to the right of the jQuery UI menu.

Enhancements
------------

The site uses one of the past 8 background images from Bing as its background
image. When viewing the description of lab 5, the "Show Solution" button is
replaced by a "Refresh Background" button, that in theory should update the
background image. If this is not working, it may be necessary to refresh
ignoring the cache (Cmd/Ctrl+Shift+R) because of peculiarities in the
implementation.
