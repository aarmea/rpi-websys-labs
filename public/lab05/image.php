<?php
// Returns one of the past $HIST images from the Bing backgrounds

// The number of past images to pick from
$HIST = 8;
// The URL for the Bing background image index
$URL = "http://www.bing.com/HPImageArchive.aspx?format=xml&idx=0&n=$HIST&mkt=en-US";
// The MIME type we expect the images to be
$MIME = "image/jpeg";

$images = array();

do {
$contents = file_get_contents($URL);

// Find all image URLs in the XML file
preg_match_all("/<url>([\/\w \.-]*)*\/?<\/url>/i", $contents, $images);
} while (sizeof($images[0]) < 1);

// Pick an image at random
$imageNum = rand(0, sizeof($images[0]));
// Strip off the tags and prepend bing.com
$imageURL = "http://www.bing.com" . substr($images[0][$imageNum], 5, -6);

// Download and send the image back to the client
// TODO: Send the URL back to the client and have it request the image by AJAX

header("Content-Type: $MIME");
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $imageURL);
curl_exec($curl);
curl_close($curl);
exit();

?>
