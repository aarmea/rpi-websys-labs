var labs = new Object();

function showLab(url) {
  var lab = labs[url];
  $("#lab-title").text("Lab " + lab.id + ": " + lab.title);
  $("#lab-description").text(lab.description);
  if (url == ".") {
    $("#lab-url").button("option", "label", "Refresh Background");
    $("#lab-url").attr("href", "#");
    $("#lab-url").click(function() {
      location.reload(true);
    });
  } else {
    $("#lab-url").button("option", "label", "Show Solution");
    $("#lab-url").attr("href", lab.url);
  }
}

function addLab(id, title, url, description) {
  var labsSection = $("#labs");

  $("<li/>").append($("<a/>", {
    text: "Lab " + id,
    href: "#",
    click: function () {
      showLab(url);
    }
  })).appendTo(labsSection);

  // Populate the global labs entry representation
  var lab = new Object();
  lab.id = id;
  lab.title = title;
  lab.url = url;
  lab.description = description;
  labs[url] = lab;
}

function addLabs(labsXML) {
  $("lab", labsXML).each(function() {
    addLab($("id", this).text(), $("title", this).text(), $("url", this).text(),
      $("description", this).text());
  });
  // Show the default lab, which is the one that is the same file as this page
  showLab(".");
  console.log(labs);
}

function setupUI() {
  $("#labs").menu();
  $("#lab-url").button();
}

$(document).ready(function() {
  setupUI();
  $.ajax("labs.xml", {
    dataType: "xml",
    success: function(data, textStatus, jqXHR) {
      console.log("AJAX load successful");
      addLabs(data);
      // Make jQuery recognize the new menu items
      $("#labs").menu("refresh");
    },
    error: function(data, textStatus, errorThrown) {
      console.log("AJAX failed due to " + textStatus);
      if (errorThrown) console.log(String(errorThrown));
      if (data) console.log(data);
    }
  });
});
