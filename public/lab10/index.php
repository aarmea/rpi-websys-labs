<?
require_once("login.php");
$login = new Login();
?>
<html>
<head>
<title>Web Systems Development Lab 10</title>
</head>
<body>

<h1>Web Systems Development Lab 10</h1>

<?
if ($login->isLoggedIn()) {
  include("menus/logged_in.php");
} else {
  include("menus/not_logged_in.php");
}
?>

<section id="lab-details">
<?
$query = $conn->query(
  "SELECT `labid`, `title`, `url`, `description` FROM `menuitems`");
$query->setFetchMode(PDO::FETCH_OBJ);
while ($lab = $query->fetch()) {
?>
  <h1>Lab <?=$lab->labid?>: <?=$lab->title?></h1>
  <p><?=$lab->description?></p>
  <a href="<?=$lab->url?>">Show Solution</a>
<?
}
?>
</section>

</body>
</html>
