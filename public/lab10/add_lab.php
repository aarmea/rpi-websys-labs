<?
require_once("login.php");
$login = new Login();

$messages = array();
$errors = array();

if (isset($_POST["add_lab"])) {
  addLab();
}

function addLab() {
  global $conn, $errors, $messages;

  // Check for simple failure conditions
  if (empty($_POST["number"]) || empty($_POST["title"]) || empty($_POST["url"])
    || empty($_POST["description"])) {
    $errors[] = "All fields are required.";
    return;
  }

  // Add (or overwrite) the lab
  $password_hash = hash("whirlpool", $_POST["password"]);
  $addLab = $conn->prepare(
    "INSERT INTO `menuitems` (`labid`, `title`, `url`, `description`)
    VALUES (:labid, :title, :url, :description)
    ON DUPLICATE KEY UPDATE
    `labid`=:labid, `title`=:title, `url`=:url, `description`=:description"
  );
  if ($addLab->execute(array(
    ":labid" => $_POST["number"], ":title" => $_POST["title"],
    ":url" => $_POST["url"], ":description" => $_POST["description"]))
  ) {
    $messages[] = sprintf("Successfully added lab %d.", $_POST["number"]);
  } else {
    $errors[] = print_r($addLab->errorInfo(), true);
  }
}

?>
<html>

<head>
<title>Add lab</title>
</head>

<body>
<h1>Web Systems Development Lab 10</h1>
<?
if ($login->isLoggedIn()) {
  include("menus/logged_in.php");
} else {
  include("menus/not_logged_in.php");
}
?>
<? if($login->isAdmin()) { ?>
<? foreach ($errors as $error) { ?>
<p>Error adding lab: <?=$error?></p>
<? } ?>
<? foreach ($messages as $message) { ?>
<p><?=$message?></p>
<? } ?>
<form method="post" action="add_lab.php">
  <!-- I use HTML5 form validation -->
  <label for="labid_input">Lab number</label>
  <input id="labid_input" type="number" name="number" min=0 required>
  <br>

  <label for="labtitle_input">Title</label>
  <input id="labtitle_input" type="text" name="title" required>
  <br>

  <label for="laburl_input">URL on server</label>
  <input id="laburl_input" type="text" name="url" required>
  <br>

  <label for="labdescription_input">Description</title>
  <textarea id="labdescription_input" name="description" required></textarea>
  <br>

  <input type="submit" name="add_lab" value="Add Lab">
</form>
<? } else { ?>
<p>You must be logged in as an administrator to add labs.</p>
<? } ?>
</body>

</html>
