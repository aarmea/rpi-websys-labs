<?
require_once("login.php");
$login = new Login();

$USERNAME_REGEX = "[a-zA-Z0-9]{2,32}";
$PASSWORD_REGEX = ".{8,}";

$messages = array();
$errors = array();

if (isset($_POST["add_user"])) {
  addUser();
}

function addUser() {
  global $USERNAME_REGEX, $PASSWORD_REGEX, $conn, $errors, $messages;

  // Check for simple failure conditions
  if (empty($_POST["username"]) || empty($_POST["password"])) {
    $errors[] = "Username and password cannot be empty.";
  }
  if (!preg_match('/'.$USERNAME_REGEX.'/', $_POST["username"])) {
    $errors[] = "Invalid username provided.";
  }
  if (!preg_match('/'.$PASSWORD_REGEX.'/', $_POST["password"])) {
    $errors[] = "Invalid password provided.";
  }
  if (!empty($errors)) return;

  // Check if the user already exists
  $username = $_POST["username"];
  $checkUser = $conn->prepare(
    "SELECT `username` FROM `users` WHERE `username` = :username"
  );
  $checkUser->setFetchMode(PDO::FETCH_OBJ);
  $checkUser->execute(array(":username" => $username));
  if ($checkUser->fetch()) {
    $errors[] = "User <code>$username</code> already exists.";
    return;
  }

  // Add the user
  $password_hash = hash("whirlpool", $_POST["password"]);
  $addUser = $conn->prepare(
    "INSERT INTO `users` (`username`, `password`, `is_admin`)
    VALUES (:username, :password, :is_admin)"
  );
  if ($addUser->execute(array(":username" => $username,
      ":password" => $password_hash, ":is_admin" => isset($_POST["admin"])))) {
    $messages[] = "Successfully added user <code>$username</code>.";
  } else {
    $errors[] = print_r($addUser->errorInfo(), true);
  }
}

?>
<html>

<head>
<title>Add user</title>
</head>

<body>
<h1>Web Systems Development Lab 10</h1>
<?
if ($login->isLoggedIn()) {
  include("menus/logged_in.php");
} else {
  include("menus/not_logged_in.php");
}
?>
<? if($login->isAdmin()) { ?>
<? foreach ($errors as $error) { ?>
<p>User creation error: <?=$error?></p>
<? } ?>
<? foreach ($messages as $message) { ?>
<p><?=$message?></p>
<? } ?>
<form method="post" action="add_user.php">
  <!-- I use HTML5 form validation -->
  <label for="username_input">Username (2 to 32 alphanumeric characters)</label>
  <input id="username_input" type="text" pattern="<?=$USERNAME_REGEX?>" name="username" required>
  <br>

  <label for="password_input">Password (at least 8 characters)</label>
  <input id="password_input" type="password" pattern="<?=$PASSWORD_REGEX?>" name="password" required autocomplete=off>
  <br>

  <label for="admin_checkbox">Create as an administrator? Administrators can add users and labs.</label>
  <input id="admin_checkbox" type="checkbox" name="admin">
  <br>

  <input type="submit" name="add_user" value="Add User">
</form>
<? } else { ?>
<p>You must be logged in as an administrator to add users.</p>
<? } ?>
</body>

</html>
