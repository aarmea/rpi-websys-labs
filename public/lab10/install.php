<?
require_once("config.php");
echo "<html><pre>";

// Connect to the SQL server and create the database if it does not exist.
try {
  $conn = new PDO(sprintf("mysql:host=%s;dbname=%s;", $config["DB_HOST"],
    $config["DB_NAME"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
  echo "Successfully connected to the database " . $config["DB_NAME"] . "\n";
} catch (PDOException $e) {
  if ($e->getCode() == 1049) {
    // The database $config["DB_NAME"] does not exist, so we need to create it.
    try {
      $conn = new PDO(sprintf("mysql:host=%s;",
        $config["DB_HOST"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
      $conn->exec("CREATE DATABASE " . $config["DB_NAME"]);
      $conn = new PDO(sprintf("mysql:host=%s;dbname=%s;", $config["DB_HOST"],
        $config["DB_NAME"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
      echo "Successfully connected to the database and created " .
        $config["DB_NAME"] . "\n";
    } catch (PDOException $e) {
      exit("<pre>Database error:\n" . $e->getMessage());
    }
  } else {
    exit("<pre>Database error:\n" . $e->getMessage());
  }
}

try {
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Create the users table
  $conn->exec(
    "CREATE TABLE `users` (
      `userid` INT NOT NULL AUTO_INCREMENT,
      `username` VARCHAR(255) NOT NULL,
      `password` VARCHAR(255) NOT NULL,
      `salt` VARCHAR(255) NOT NULL,
      `is_admin` BOOL DEFAULT FALSE,
      PRIMARY KEY (`userid`)
    ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  );
  echo "Successfully created table 'users'\n";

  // Create the menuitems table
  $conn->exec(
    "CREATE TABLE `menuitems` (
      `labid` INT NOT NULL,
      `title` VARCHAR(255) NOT NULL,
      `url` VARCHAR(255) NOT NULL,
      `description` TEXT NOT NULL,
      PRIMARY KEY (`labid`)
    ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  );
  echo "Successfully created table 'menuitems'\n";

  // Create a default admin user
  $default_password = "admin";
  $default_password_hash = hash("whirlpool", $default_password);
  $conn->exec(
    "INSERT INTO `users` VALUES (
      0, 'admin', '$default_password_hash', '', TRUE
    );"
  );
  echo "Successfully created a default admin user\n";

  // Create default lab entries
  $conn->exec(
    "INSERT INTO `menuitems` VALUES (
      2, 'Semantic HTML and XML with CSS', '/lab02/',
      'CSS is used to style a HTML and XML playlist of favorite songs.'
    ), (
      3, 'JavaScript and the Document Object Model', '/lab03/',
      'JavaScript is used to print the DOM structure and dynamically add CSS styles on mouseover.'
    ), (
      4, 'JSON and AJAX', '/lab04/', 'AJAX is used to request and parse an asynchronously loaded JSON file that contains a playlist of songs as an extension of Lab 2.'
    );"
  );
  echo "Successfully populated default lab entries";

} catch (PDOException $e) {
  exit("Database error:\n" . $e->getMessage());
}

?>
