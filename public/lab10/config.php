<?
// This configuration is valid for servers created from the Vagrant
// configuration at https://github.com/aarmea/RPI-Study-Solutions. Customize the
// settings for your installation before running.
$config = array(
  "DB_HOST" => "localhost",
  "DB_NAME" => "websysquiz2",
  "DB_USERNAME" => "myadmin",
  "DB_PASSWORD" => "myadmin"
);
?>
