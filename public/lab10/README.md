Lab 10/Quiz 2: Databased Lab Index
==================================

Usage notes
-----------

* Database settings are in `config.php`. Adjust this before running on your
  server.
* Initialize the database by opening `install.php`. A corresponding
  `uninstall.php` deletes the created tables.
* The install process generates a default administrative user with username
  `admin` and password `admin`. Since there is no way to edit users, there is no
  way to remove the default administrator or change its password.
* Attempting to add a lab whose number already exists in the database will
  result in that lab being overwritten.
