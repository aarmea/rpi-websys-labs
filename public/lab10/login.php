<?
require_once("config.php");

// Connect to the SQL server and throw an error if it does not exist.
try {
  $conn = new PDO(sprintf("mysql:host=%s;dbname=%s;", $config["DB_HOST"],
    $config["DB_NAME"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
} catch (PDOException $e) {
  exit("<pre>Database error:\n" . $e->getMessage());
}

class Login {
  public $errors = array();

  private $logged_in = false;

  public function __construct() {
    session_start();

    if (isset($_GET["logout"])) {
      $this->logout();
    } elseif (!empty($_SESSION["username"]) && $_SESSION["logged_in"]) {
      $this->sessionDataLogin();
    } elseif (isset($_POST["login"])) {
      $this->postDataLogin();
    }
  }

  public function isLoggedIn() {
    return $this->logged_in;
  }

  public function username() {
    return ($this->logged_in) ? $_SESSION["username"] : "";
  }

  public function isAdmin() {
    return ($this->logged_in) ? $_SESSION["admin"] : false;
  }

  private function sessionDataLogin() {
    $this->logged_in = true;
  }

  private function postDataLogin() {
    // If we have an incomplete login request, don't do anything
    if (empty($_POST["username"]) || empty($_POST["password"])) {
      $this->errors[] = "Username and password cannot be blank.";
      return;
    }

    // Query the database for the user
    global $conn;
    $query = $conn->prepare(
      "SELECT `username`, `password`, `salt`, `is_admin` FROM `users`
      WHERE `username` = :username"
    );
    $query->setFetchMode(PDO::FETCH_OBJ);
    $query->execute(array(":username" => $_POST["username"]));
    $user = $query->fetch();

    // Check that the user exists and that the password is correct
    if (!$user || hash("whirlpool", $_POST["password"]) != $user->password) {
      $this->errors[] = "Username or password is incorrect.";
      return;
    }

    // Verification successful, mark as such and store the session
    $this->logged_in = true;
    $_SESSION["username"] = $user->username;
    $_SESSION["logged_in"] = true;
    $_SESSION["admin"] = $user->is_admin;
  }

  private function logout() {
    $_SESSION = array();
    session_destroy();
    $this->logged_in = false;
  }
}
?>
