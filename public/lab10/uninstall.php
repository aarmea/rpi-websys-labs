<?
require_once("config.php");
echo "<html><pre>";

try {
  $conn = new PDO(sprintf("mysql:host=%s;", $config["DB_HOST"]),
    $config["DB_USERNAME"], $config["DB_PASSWORD"]);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Successfully connected to the database\n";

  $conn->exec("DROP DATABASE " . $config["DB_NAME"]);
  echo "Successfully deleted database " . $config["DB_NAME"] . "\n";
} catch (PDOException $e) {
  exit("Database error:\n" . $e->getMessage());
}

?>
