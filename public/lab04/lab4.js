function addSong(song) {
  var songElement = $("<tr/>", {
    "class": "song"
  });

  // The order that we want each property to appear in the table. We can handle
  // receiving the properties in any order.
  songPropertyTypes = [
    "albumart",  "title", "artist", "album", "release", "genre", "url"];

  // Iterate over the expected properties and add each to the playlist element
  // (table row).
  for (var i = 0; i < songPropertyTypes.length; ++i) {
    var propertyType = songPropertyTypes[i];
    var propertyValue = song[propertyType];

    var element = $("<td/>", {"class": propertyType});
    switch(propertyType) {
      case "albumart":
        $("<img/>", {
          "src": propertyValue
        }).appendTo(element);
        break;
      case "url":
        $("<a/>", {
          "href": propertyValue,
          html: propertyValue
        }).appendTo(element);
        break;
      default:
        element.html(propertyValue);
    }
    element.appendTo(songElement);
  }

  // Append the table row to the table in the DOM.
  songElement.appendTo($("#songs"));
}

function loadSongs(jsonURL) {
  $.getJSON(jsonURL)
    .done(function(jsonSongs) {
      // Getting and parsing the JSON was successful, so we can proceed with
      // adding it to the DOM.
      console.log("callback received");
      $("#content").show();
      $("#site").remove();
      $.each(jsonSongs, function(songsParent, songs) {
        $.each(songs, function(i, song) {
          addSong(song);
        });
      });
    })
    .fail(function(jqxhr, textStatus, error) {
      console.log("Request failed: " + textStatus + ", " + error + ", \n"
        + jqxhr.responseText);
    })
  ;
}

$(document).ready(function() {
  $("#site").click(function() {
    loadSongs("lab4.json");
  });
});
