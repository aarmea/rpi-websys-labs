Lab 6: Front-end Optimization
=============================

Albert Armea <armeaa@rpi.edu>

Part 1
------

I use jQuery event delegation using the
[`.on()` method](http://api.jquery.com/on/) to all `<ul>` tags. I specify that a
method that removes elements is run onclick if the child element is an `<li>`.

Part 2
------

### Optimization 1: Generate all list elements using JavaScript

In the provided code, 5000 list elements were defined in HTML and another 5000
were added using JavaScript. I removed the HTML-defined elements and had the
JavaScript generate 10000 elements instead. This is a significant savings (351
KB) that should decrease the loading time.

### Optimization 2: Use a solid color background instead of an image

In the provided code, the background was pulled from an
[image](http://www.salasphotography.com/_wizardimages/lightblue_square.jpg)
on an external site. Since this image was just a solid color, I replaced it with
its corresponding background color to save 13.5 KB.


### Optimization 3: Create and populate the list outside of the DOM

In the provided code, the list was populated directly on the DOM, causing the
page to be re-rendered several times during population. This process caused
loading to take around 2.45 seconds on my machine. I modified the JavaScript to
create a `<ul>` outside the DOM where the list elements were added. I only add
this `<ul>` to the DOM after all of the elements have been added. The new code
takes around 1.8 seconds to load on my machine.

### Optimization 4: Create the list element only once, then clone it

Optimization 3 uses jQuery to create a new `<li>` element 10000 times. In this
optimization, I create the `<li>` outside of the loop and clone it within the
loop. This allows loading time to drop from around 1.8 seconds to around 1.65
seconds.
