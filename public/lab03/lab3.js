function explore(target, tag, depth) {
  // Ignore implicit #TEXT tags
  if (tag.nodeName[0] == "#") return;

  // Part 1: Print the node and its depth
  target.innerHTML += Array(depth+1).join("-") + tag.nodeName.toLowerCase() +
    "\n";

  // Part 2: Add onclick event listeners
  tag.addEventListener("click", function() {
    alert(tag.nodeName);
  });

  // Keep exploring this node's children
  var children = tag.childNodes;
  if (typeof children === 'undefined') return;
  for (var i = 0; i < children.length; ++i) {
    explore(target, children.item(i), depth+1);
  }
}

window.onload = function() {
  // Start Part 1 and Part 2
  var target = document.getElementById("info");
  target.innerHTML += "\n\n";
  explore(target, document.getElementsByTagName("html").item(0), 0);

  // Part 3: Add quote at the end
  var quote = document.createElement("p");
  quote.innerHTML = "I know what you did last summer.";
  quote.className += " quote";
  document.body.appendChild(quote);
  var attribution = document.createElement("div");
  attribution.innerHTML = "The NSA";
  attribution.className += " attribution";
  quote.appendChild(attribution);

  // Part 3: Add onmouseover and onmouseout listeners
  divs = document.getElementsByTagName("div");
  for (var i = 0; i < divs.length; i++) {
    divs[i].onmouseover = function(e) {
      this.id = "myHover";
    };
    divs[i].onmouseout = function(e) {
      this.id = "";
    };
  }
}
