<?
// This configuration is valid for servers created from the Vagrant
// configuration at https://github.com/ymainier/vagrant-lamp. Customize the
// settings for your installation before running.
$config = array(
  "DB_HOST" => "localhost",
  "DB_NAME" => "websyslab9",
  "DB_USERNAME" => "myadmin",
  "DB_PASSWORD" => "myadmin"
);
?>
