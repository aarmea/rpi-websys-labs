<?
require_once("config.php");
echo "<html><pre>";

// Connect to the SQL server and create the database if it does not exist.
try {
  $conn = new PDO(sprintf("mysql:host=%s;dbname=%s;", $config["DB_HOST"],
    $config["DB_NAME"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
  echo "Successfully connected to the database " . $config["DB_NAME"] . "\n";
} catch (PDOException $e) {
  if ($e->getCode() == 1049) {
    // The database $config["DB_NAME"] does not exist, so we need to create it.
    try {
      $conn = new PDO(sprintf("mysql:host=%s;",
        $config["DB_HOST"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
      $conn->exec("CREATE DATABASE " . $config["DB_NAME"]);
      $conn = new PDO(sprintf("mysql:host=%s;dbname=%s;", $config["DB_HOST"],
        $config["DB_NAME"]), $config["DB_USERNAME"], $config["DB_PASSWORD"]);
      echo "Successfully connected to the database and created " .
        $config["DB_NAME"] . "\n";
    } catch (PDOException $e) {
      exit("<pre>Database error:\n" . $e->getMessage());
    }
  } else {
    exit("<pre>Database error:\n" . $e->getMessage());
  }
}

try {
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // Create the courses table
  $conn->exec(
    "CREATE TABLE `courses` (
      `crn` INT(11) NOT NULL,
      `prefix` VARCHAR(4) NOT NULL,
      `number` SMALLINT(4) NOT NULL,
      `title` VARCHAR(255) NOT NULL,
      `section` INT NOT NULL,
      `year` YEAR NOT NULL,
      PRIMARY KEY (`crn`)
    ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  );
  echo "Successfully created table 'courses'\n";

  // Create the students table
  $conn->exec(
    "CREATE TABLE `students` (
      `rin` INT(9) NOT NULL,
      `first name` VARCHAR(100) NOT NULL,
      `last name` VARCHAR(100) NOT NULL,
      `address1` VARCHAR(255) NOT NULL,
      `address2` VARCHAR(255) NOT NULL,
      `city` VARCHAR(255) NOT NULL,
      `state` CHAR(2) NOT NULL,
      `zip` INT(5) NOT NULL,
      `zip4` INT(9) NOT NULL,
      PRIMARY KEY (`rin`)
    ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  );
  echo "Successfully created table 'students'\n";


  // Create the grades table
  $conn->exec(
    "CREATE TABLE `grades` (
      `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `crn` INT(11) NOT NULL,
      `rin` INT(9) NOT NULL,
      `grade` INT(3) NOT NULL,
      FOREIGN KEY (`crn`) REFERENCES `courses`(`crn`)
      ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (`rin`) REFERENCES `students`(`rin`)
      ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  );
  echo "Successfully created table 'grades'\n";
} catch (PDOException $e) {
  exit("Database error:\n" . $e->getMessage());
}

?>
