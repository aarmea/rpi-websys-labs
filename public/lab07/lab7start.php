<?php
  abstract class Operation {
    protected $operand_1;
    protected $operand_2;
    public function __construct($o1, $o2) {
      // Make sure we're working with numbers...
      if (!is_numeric($o1) || !is_numeric($o2)) {
        throw new Exception('Non-numeric operand.');
      }

      $this->operand_1 = $o1;
      $this->operand_2 = $o2;
    }
    public abstract function operate();
    public abstract function getEquation();
  }

  class Addition extends Operation {
    public function operate() {
      return $this->operand_1 + $this->operand_2;
    }
    public function getEquation() {
      return $this->operand_1 . ' + ' . $this->operand_2 . ' = ' . $this->operate();
    }
  }

  class Subtraction extends Operation {
    public function operate() {
      return $this->operand_1 - $this->operand_2;
    }
    public function getEquation() {
      return $this->operand_1 . ' - ' . $this->operand_2 . ' = ' . $this->operate();
    }
  }

  class Multiplication extends Operation {
    public function operate() {
      return $this->operand_1 * $this->operand_2;
    }
    public function getEquation() {
      return $this->operand_1 . ' * ' . $this->operand_2 . ' = ' . $this->operate();
    }
  }

  class Division extends Operation {
    public function operate() {
      return $this->operand_1 / $this->operand_2;
    }
    public function getEquation() {
      if ($this->operand_2 == 0) {
        throw new Exception("Division by zero.");
      }
      return $this->operand_1 . ' / ' . $this->operand_2 . ' = ' . $this->operate();
    }
  }

// Check to make sure that POST was received
// upon initial load, the page will be sent back via the initial GET at which time
// the $_POST array will not have values - trying to access it will give undefined message

  if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $o1 = $_POST['op1'];
    $o2 = $_POST['op2'];
  }
  $err = Array();

  try {
    if (!isset($_POST['operation'])) {
      throw new Exception("Operation not provided");
    }
    switch ($_POST['operation']) {
    case 'Add':
      $op = new Addition($o1, $o2);
      break;
    case 'Subtract':
      $op = new Subtraction($o1, $o2);
      break;
    case 'Multiply':
      $op = new Multiplication($o1, $o2);
      break;
    case 'Divide':
      $op = new Division($o1, $o2);
      break;
    default:
      throw new Exception("Unknown operation: " . $_POST['operation']);
    }
  } catch (Exception $e) {
    $err[] = $e->getMessage();
  }
?>

<!doctype html>
<html>
<head>
<title>Lab 7</title>
</head>
<body>
  <pre id="result">
  <?php
    if (isset($op)) {
      try {
        echo $op->getEquation();
      }
      catch (Exception $e) {
        $err[] = $e->getMessage();
      }
    }

    foreach($err as $error) {
        echo $error . "\n";
    }
  ?>
  </pre>
  <form method="post" action="lab7start.php">
    <input type="text" name="op1" id="name" value="" />
    <input type="text" name="op2" id="name" value="" />
    <br/>
    <!-- Only one of these will be set with their respective value at a time -->
    <input type="submit" name="operation" value="Add" />
    <input type="submit" name="operation" value="Subtract" />
    <input type="submit" name="operation" value="Multiply" />
    <input type="submit" name="operation" value="Divide" />
  </form>
</body>
</html>

