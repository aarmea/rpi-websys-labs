Lab 7: PHP, OOP, and Input Handling
===================================

Albert Armea <armeaa@rpi.edu>

Part 1
------

I implemented Subtraction, Multiplication, and Division classes that inherit
Operation in the same manner as Addition. I had the Division class throw an
exception in the case of division by zero so that the error is reported in the
same format as the other errors rather than a generic PHP warning.

Part 2
------

I modified the provided form so that each submit button's name is "operation" so
that I can check what operation was requested within a switch/case group. I
throw exceptions if the "operation" variable is not set or if it contains an
invalid operation.

Other stuff
-----------

When one of the operation buttons is clicked, the browser sends a POST request
to the server containing the operation name and operands as POST parameters.
Then execution begins from the beginning of the PHP file again: it enters the
switch/case group to check the operation and creates the appropriate Operation
subclass, sets errors if present, and outputs the HTML with the equation by
calling the getEquation method on the Operation subclass or the generated
error(s).

If I used $_GET to implement the application, it would be necessary to pass in
the operands and the operation as $_GET variables, which is specified in the
`form` by replacing `method="post"` with `method="get"`. On the server side, I
would need to replace all instances of $_POST with $_GET.

I used an alternate method to determine which button was pressed using a
case/switch loop. See the section titled "Part 2" for details.
